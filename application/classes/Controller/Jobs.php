<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Jobs extends Controller_Template {

    private $session;
    private $access_token;
    private $logged_user;
    
    private $pending_count = 0;
    private $processing_count = 0;
    private $completed_count = 0;
    private $junks_count = 0;
    private $transactions;
    private $db_id;
    private $is_admin = false;
    private $response_editable = true;
    
    public function __construct(\Request $request, \Response $response) {
        parent::__construct($request, $response);
        $this->session = Session::instance();
        $this->access_token = $this->session->get('admin_access_token');
        $this->logged_user = $this->session->get('user_id');
        $this->db_id = $this->session->get('db_id');
        if($this->session->get('identity') == 1){
            $this->header_view = 'included/home_header';
            $this->is_admin = true;
        }
        else{
            $this->header_view = 'included/vendor_header';
        }
        $this->transactions = new Model_Transactions($this->db_id, $this->access_token);
    }
    
    public function action_index(){
        $title = "Jobs, Errand Services for ".$this->logged_user;
        $this->renderIt($title, "");
    }
    
    public function action_view(){
        $job_id = $this->request->param("id");
        $title = "Preview Job ".$job_id;
        $alert_type = "";
        $alert_message = "";
        
        $tranx = $this->transactions->viewTransaction($job_id);
        $tabs_array = array("brief"=>"active", "response"=>"", "actions"=>"", "payment"=>"");
        $form_data = array("errand_cost"=> 0, "cash_handling_cost"=>0, "tax"=>0, 
            "pickup_date"=>date('d-m-Y'), "delivery_date"=>date('d-m-Y'),
            "what_we_pay_you"=>0, "our_commission"=>0, "total"=>0);
        if($tranx){
            $can_display = true;
        }
        if($this->request->post("submit_bill")){
            $tabs_array['brief'] = "";
            $tabs_array['response'] = "active";
            $errand_cost = $this->request->post('errand_cost');
            $cash_handling_cost = $this->request->post('cash_handling_cost');
            $tax = $this->request->post('tax');
            $pickup_date = $this->request->post('pickup_date');
            $delivery_date = $this->request->post('delivery_date');
            $what_we_pay_you = $this->request->post('what_we_pay_you');
            $our_commission = $this->request->post('our_commission');
            $form_data = $this->request->post();
            //check if the dates are okay.
            $from_d = date_create($pickup_date);
            $to_d = date_create($delivery_date);
            $d_diff = date_diff($from_d, $to_d);
            $diff = intval($d_diff->format("%R%a"));
            if($diff < 0){
                //complain o
                $alert_type = "danger";
                $alert_message = "Invalid Dates Provided";
            }
            else{
                $response = json_decode($this->transactions->billTransaction($job_id, 
                                json_encode($form_data)));
                if($response->status == 1){
                    if($response->data->code == 1){
                        //success
                        $alert_type = "success";
                        $alert_message = $response->data->description;
                        $this->response_editable = false;
                    }
                    else{
                        $alert_type = "warning";
                        $alert_message = $response->data->description;
                    }
                }
            }
        }
        else{
            if($tranx->bill->total > 0){
                $this->response_editable = false;
                $form_data = array("errand_cost"=> $tranx->bill->errand_cost, "cash_handling_cost"=>
                    $tranx->bill->cash_handling_cost, "tax"=>$tranx->bill->tax, 
                    "pickup_date"=>$tranx->bill->pickup_date, "delivery_date"=>$tranx->bill->delivery_date,
                    "what_we_pay_you"=>$tranx->bill->what_we_pay_you, "our_commission"=>
                    $tranx->bill->our_commission, "total"=>$tranx->bill->total);
            }
        }
        $this->renderIt($title, View::factory("pages/sub/view_job")
                ->set("transaction", $tranx)
                ->set("can_display", $can_display)
                ->set("is_admin", $this->is_admin)
                ->set("tabs", $tabs_array)
                ->set("form_data", $form_data)
                ->set("alert_type", $alert_type)
                ->set("alert_message", $alert_message)
                ->set("response_editable", $this->response_editable));
    }
    
    public function action_pending(){
        $title = "Pending Jobs, Errand Services for ".$this->logged_user;
        $this->renderIt($title, View::factory("pages/sub/jobs")
                ->set("task", "pending"));
    }
    
    public function action_processing(){
        $title = "Processing Jobs, Errand Services for ".$this->logged_user;
        $this->renderIt($title, View::factory("pages/sub/jobs")
                ->set("task", "processing"));
    }
    
    public function action_completed(){
        $title = "Completed Jobs, Errand Services for ".$this->logged_user;
        $this->renderIt($title, View::factory("pages/sub/jobs")
                ->set("task", "completed"));
    }
    
    public function action_junks(){
        $title = "Junk Jobs, Errand Services for ".$this->logged_user;
        $this->renderIt($title, View::factory("pages/sub/jobs")
                ->set("task", "junk"));
    }
    
    private function renderIt($title, $sub_view){
        $arr_tranx = $this->transactions->getTransactionSummary();
        $this->pending_count = array_key_exists("PENDING", $arr_tranx)?$arr_tranx['PENDING']:0;
        $this->processing_count = array_key_exists("PROCESSING", $arr_tranx)?$arr_tranx['PROCESSING']:0;
        $this->completed_count = array_key_exists("COMPLETED", $arr_tranx)?$arr_tranx['COMPLETED']:0;
        $this->completed_count = array_key_exists("JUNK", $arr_tranx)?$arr_tranx['JUNK']:0;

            
        $this->view = View::factory('template/home.tpl')
                ->bind('title', $title);

        
        $this->view->set("dynamic_view", View::factory('pages/jobs')
                    ->set("pending_jobs", $this->pending_count)
                    ->set("processing_jobs", $this->processing_count)
                ->set("junks", $this->junks_count)
                    ->set("completed_jobs", $this->completed_count)
                ->set("sub_view", $sub_view));

        $this->view->set("home_header_content", View::factory($this->header_view)
                ->bind('user_type', $this->logged_user)
                ->set("errands_request_active", "active"));

        //include the footer
        $this->view->set("footer", View::factory('included/footer'));


        //set all the modal dat will be used by this action here
        $this->view->set("create_user_modal_content", View::factory('included/create_user_modal'));
        $this->view->set("create_state_modal_content", View::factory('included/create_state_modal'));

        $this->response->body($this->view); //will comment this later  
    }
    
}
