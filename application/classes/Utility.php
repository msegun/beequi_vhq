<?php

class Utility{
    
    public static function dateFormatter($date){
        $separate = explode($date, "T");
        $date = $separate[0];
        $ret_date = date("Y-m-d", $date);
    }
    
    public static function uploadVendorImage($image, $name){
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png')))
        {
            return FALSE;
        }
 
        $directory = DOCROOT.'uploads/vendors/';
 
        if ($file = Upload::save($image, NULL, $directory))
        {
            $filename = strtolower($name).'.jpg';
 
            $img = Image::factory($file);
            $img->resize(150, 150)
                    ->save($directory.$filename);
            unlink($file);
 
            return 'uploads/vendors/'.$filename;
        }
 
        return FALSE;
    }
}
