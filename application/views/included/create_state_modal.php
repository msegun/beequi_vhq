<!-- Modal -->
<div class="modal fade slide-up disable-scroll" id="statemodalSlideUp" tabindex="-1" role="dialog" aria-hidden="false">
   <div class="modal-dialog ">
      <div class="modal-content-wrapper">
         <div class="modal-content">
            <div class="modal-header clearfix text-left">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
               </button>
               <h5>Add a <span class="semi-bold">New State</span></h5>
            </div>
            <div class="modal-body">

               <form id="form-register" class="p-t-15" role="form" action="<?php echo SITE_PATH; ?>states" method="post">
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group form-group-default">
                           <label>State Name</label>
                           <input type="text" name="state_name" placeholder="enter state name" class="form-control" required>
                        </div>
                     </div>

                  </div>



                  <input type="hidden" name="isAddState" value="yes" />
                  <button class="btn btn-primary btn-cons m-t-10" type="submit">Add State</button>
               </form>

            </div>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
</div>