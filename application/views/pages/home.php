      <div class="page-content-wrapper">
        <!-- START PAGE CONTENT -->
        <div class="content">
          <!-- START JUMBOTRON -->
          <div class="jumbotron lg vertical center bg-white column-w4 vertical-bottom" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20 full-height">
              <div class="inner text-center p-t-50 m-t-80 full-height">
                <div class="m-b-10">
                  <h2 class=" p-t-10 inline"><?= @$business_name ?></h2>
                </div>
   
          <div class="container-fluid container-fixed-lg">
            <br>
            <div class="row icon-set-preview">
              <div class="col-md-4">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <div class="panel-title text-danger">Pending Jobs
                    </div>
                  </div>
                  <div class="panel-body text-center">
                      <h2><a href="<?= SITE_PATH ?>jobs/pending"><?= @$pending_jobs ?></a></h2>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <div class="panel-title text-warning">Processing Jobs
                    </div>
                  </div>
                  <div class="panel-body text-center">
                      <h2><a href="<?= SITE_PATH ?>jobs/processing"><?= @$processing_jobs ?></a></h2>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <div class="panel-title text-success">Completed Jobs
                    </div>
                  </div>
                  <div class="panel-body text-center">
                      <h2><a href="<?= SITE_PATH ?>jobs/completed"><?= @$completed_jobs ?></a></h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
                  
                  
                </div>
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
        </div>
        <!-- END PAGE CONTENT -->
      </div>