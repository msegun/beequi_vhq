<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Tables extends Controller {
    
    private $table;
    private $session;
    private $access_token;
    private $logged_user;
    private $db_id;
    
    public function __construct(\Request $request, \Response $response) {
        parent::__construct($request, $response);
        $this->session = Session::instance();
        $this->access_token = $this->session->get('admin_access_token');
        $this->logged_user = $this->session->get('user_id');
        $this->db_id = $this->session->get('db_id');
        $this->table = new Model_Tables($this->db_id, $this->access_token);
    }
    
    public function action_customers(){
        $data = array();
        if($this->request->post()){
            $current = $this->request->post("current");
            $rowCount = $this->request->post("rowCount");
            $searchPhrase = $this->request->post("searchPhrase");
            $data = $this->table->fetchManageStates($current, $rowCount, $searchPhrase);
        }
        echo json_encode($data);
    }
    
    public function action_transaction(){
        $data = array();
        $type = strtoupper($this->request->param("id"));
        if($this->request->post()){
            $current = $this->request->post("current");
            $rowCount = $this->request->post("rowCount");
            $searchPhrase = $this->request->post("searchPhrase");
            $data = $this->table->fetchTransaction($type, $current, $rowCount, $searchPhrase);
        }
        echo json_encode($data);
    }

}

