<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Vendors extends Controller_Template {

    protected $session;
    protected $access_token;
    protected $logged_user;
    
    public function __construct(\Request $request, \Response $response) {
        parent::__construct($request, $response);
        $this->session = Session::instance();
        $this->access_token = $this->session->get('admin_access_token');
        $this->logged_user = $this->session->get('user_id');
        if($this->session->get('identity') == 1){
            $this->header_view = 'included/home_header';
        }
        else{
            $this->header_view = 'included/vendor_header';
        }
    }
    
    public function action_index(){
        if(!$this->access_token){
            HTTP::redirect(SITE_PATH . "logout");
        }
        if ($this->request->post('isAddVendor')) {
            $vendor_name = $this->request->post('vendor_name');
            $vendor_email = $this->request->post('vendor_email');
            $vendor_password = $this->request->post('vendor_pass');
            if (empty($vendor_name) || empty($vendor_email) || empty($vendor_password)) {
                $notification['status'] = 0;
                $notification['message'] = "Empty field(s)";
            } else { // send the details to the API for processing
                // This uses POST
                $request = Request::factory(API_CALL_URL.'vendor/add/' . $this->access_token,
                        array('strict_redirect' => FALSE, 'follow' => TRUE))
                        ->method(Request::POST)
                        ->post(array('vendor_name' => $vendor_name, 'vendor_email' => $vendor_email, 'password' => $vendor_password))
                        ->execute();
                $response = json_decode($request->body(), TRUE);


                $add_status = ($response['status']);

                if(!$add_status){
                        $add_message = ($response['data']['errmsg']);
                }else{
                        $add_message = ("Operation Successful");
                }

                $notification['status'] = $add_status;
                $notification['message'] = $add_message;
            }
        }
        else{

            $notification['status'] = 5;
            $notification['message'] = "";

	}
        
        //manage vendors
        //display the vendor lists and their pagination if we have a pagination setted in the URL
        if ($this->request->query('page')) {
                $page = $this->request->query('page');
        } else {
                $page = 1;
        }

        $curl_response = Model::factory('Curl')->get(API_CALL_URL.'vendor/list/' . $this->access_token . '/#/' . $page);

        $curl_response = json_decode($curl_response, TRUE);
        $vendors_data = array();
        $vendors_response['status'] = $curl_response['status'];
        if($vendors_response['status']){
            $vendors_data = $curl_response['data']['docs'];
        }
        $title = "Vendors List";

        $this->view = View::factory('template/home.tpl')
                ->bind('title', $title);

        $this->view->set("dynamic_view", View::factory('pages/vendors_list')
                ->bind('notification', $notification)
                ->bind("vendors_list", $vendors_data));

        $this->view->set("home_header_content", View::factory($this->header_view)
                ->bind('user_type', $this->logged_user)
                ->set('manage_vendors_active', 'active'));


        //include the footer
        $this->view->set("footer", View::factory('included/footer'));


        //set all the modal dat will be used by this action here
        $this->view->set("create_user_modal_content", View::factory('included/create_user_modal'));
        $this->view->set("create_state_modal_content", View::factory('included/create_state_modal'));

        $this->response->body($this->view); //will comment this later
    }
    
    public function action_profile(){
        $vendor_id = str_replace("#", "", $this->request->param("id"));
        $notification['status'] = 5;
        $notification['message'] = "";
        
        if($this->request->post('company_update')){
            if(!empty($_FILES['company_logo']['name'])){
                //echo "here";die;
                $logo_path = Utility::uploadVendorImage($_FILES['company_logo'], $vendor_id);
            }
            else{
                $logo_path = $this->request->post('company_logo_url');
            }
            
            $array_to_post = $this->request->post();
            $array_to_post['company_logo_url'] = $logo_path;
            $request = Request::factory(API_CALL_URL.'vendor/update/profile/' . $this->access_token.
                    '/'.$vendor_id,
                    array('strict_redirect' => FALSE, 'follow' => TRUE))
                    ->method(Request::POST)
                    ->post($array_to_post)
                    ->execute();
            $response = json_decode($request->body(), FALSE);
            if($response->status){
                $notification['status'] = 1;
                $notification['message'] = "Basic profile update Operation Successful";
            }
            else{
                $notification['status'] = 0;
                $notification['message'] = "Basic profile update Operation Failed";
            }            
        }
        
        if($this->request->post('add_state')){
            $state_to_add = $this->request->post('state_to_add');
            if(!empty($state_to_add)){
                // This uses POST
                $request = Request::factory(API_CALL_URL.'vendor/add/state_of_operations/' . $this->access_token.
                        '/'.$vendor_id,
                        array('strict_redirect' => FALSE, 'follow' => TRUE))
                        ->method(Request::POST)
                        ->post(array('state' => $state_to_add))
                        ->execute();
                $response = json_decode($request->body(), FALSE);
                if($response->status){
                    $notification['status'] = 1;
                    $notification['message'] = "Adding of State Operation Successful";
                }
                else{
                    $notification['status'] = 0;
                    $notification['message'] = "Adding of State Operation Failed";
                }
            }
            else{
                $notification['status'] = 0;
                $notification['message'] = "Adding of State Operation Failed";
            }
        }
        
        if($this->request->post("remove_state")){
            $state_to_remove = $this->request->post('state_to_remove');
            if(!empty($state_to_remove)){
                $request = Request::factory(API_CALL_URL.'vendor/remove/state_of_operations/' . $this->access_token.
                        '/'.$vendor_id,
                        array('strict_redirect' => FALSE, 'follow' => TRUE))
                        ->method(Request::POST)
                        ->post(array('state' => $state_to_remove))
                        ->execute();
                $response = json_decode($request->body(), FALSE);
                if($response->status){
                    $notification['status'] = 1;
                    $notification['message'] = "Removing of State Operation Successful ".$state_to_remove;
                }
                else{
                    $notification['status'] = 0;
                    $notification['message'] = "Removing of State Operation Failed";
                }
            }
            else{
                $notification['status'] = 0;
                $notification['message'] = "Removing of State Operation Failed";
            }
        }
        
        $curl_response = Model::factory('Curl')->get(API_CALL_URL.'vendor/view/'.$this->access_token.'/'.$vendor_id);
        $curl_response_basic = json_decode($curl_response);
        if(!$curl_response_basic->status){
            HTTP::redirect(SITE_PATH."vendors");
        }
        $curl_response = Model::factory('Curl')->get(API_CALL_URL.'vendor/view/state_of_operations/'.$this->access_token.'/'.$vendor_id);
        $curl_response_state_of_operations = json_decode($curl_response);
        $curl_response = Model::factory('Curl')->get(API_CALL_URL.'vendor/view/logs/'.$this->access_token.'/'.$vendor_id);
        $curl_response_logs = json_decode($curl_response);
        $curl_response = Model::factory('Curl')->get(API_CALL_URL.'states/list/#');
        $curl_response_states_list = json_decode($curl_response, FALSE);
        $title = "View ".$curl_response_basic->data->vendor_name;

        //var_dump($curl_response_state_of_operations);die;
        $view = $this->render_profile($title, $notification, $curl_response_basic, $curl_response_state_of_operations,
            $curl_response_logs, $curl_response_states_list, $vendor_id);
         $this->response->body($view); //will comment this later

    }
    
    protected function render_profile($title, $notification, $curl_response_basic, $curl_response_state_of_operations,
            $curl_response_logs, $curl_response_states_list, $vendor_id, $menu='manage_vendors_active', $admin=true){

        $this->view = View::factory('template/home.tpl')
                ->bind('title', $title);

        $this->view->set("dynamic_view", View::factory('pages/view_vendor')
                ->bind('notification', $notification)
                ->bind("vendor_details", $curl_response_basic->data)
                ->set("vendor_state_of_operations", $curl_response_state_of_operations->data)
                ->set("vendor_logs", $curl_response_logs->data)
                ->set("states_list", $curl_response_states_list->data)
                ->set("vendor_id", $vendor_id)
                ->set("is_admin", $admin));

        $this->view->set("home_header_content", View::factory($this->header_view)
                ->bind('user_type', $this->logged_user)
                ->set($menu, 'active'));


        //include the footer
        $this->view->set("footer", View::factory('included/footer'));


        //set all the modal dat will be used by this action here
        $this->view->set("create_user_modal_content", View::factory('included/create_user_modal'));
        $this->view->set("create_state_modal_content", View::factory('included/create_state_modal'));

        return $this->view;
    }
    
    public function action_status(){
        $vendor_id = str_replace("#", "", $this->request->param("id"));
        $new_status = $this->request->param("id2");
        $request = Request::factory(API_CALL_URL.'vendor/status/change/' . $this->access_token.
            '/'.$vendor_id,
            array('strict_redirect' => FALSE, 'follow' => TRUE))
            ->method(Request::POST)
            ->post(array('status' => $new_status))
            ->execute();
        HTTP::redirect(SITE_PATH."vendors/profile/".$vendor_id);
    }
}