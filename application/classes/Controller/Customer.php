<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Customer extends Controller_Template {

    private $session;
    
    public function __construct(\Request $request, \Response $response) {
        parent::__construct($request, $response);
        $this->session = Session::instance();
        $this->access_token = $this->session->get('admin_access_token');
        $this->logged_user = $this->session->get('user_id');
        if($this->session->get('identity') == 1){
            $this->header_view = 'included/home_header';
        }
        else{
            $this->header_view = 'included/vendor_header';
        }
    }
    
    public function action_index(){
        if(!$this->session->get('admin_access_token')){
            HTTP::redirect(SITE_PATH.'logout');
        }

        $title = "Welcome ".$this->logged_user;

        $this->view = View::factory('template/home.tpl')
                ->bind('title', $title);

        $this->view->set("dynamic_view", View::factory('pages/customers_list')
                ->set("token", $this->access_token));

        $this->view->set("home_header_content", View::factory($this->header_view)
                ->bind('user_type', $this->logged_user)
                ->set("manage_customer_active", "active"));

        //include the footer
        $this->view->set("footer", View::factory('included/footer'));


        //set all the modal dat will be used by this action here
        $this->view->set("create_user_modal_content", View::factory('included/create_user_modal'));
        $this->view->set("create_state_modal_content", View::factory('included/create_state_modal'));

        $this->response->body($this->view); //will comment this later
    }
}
