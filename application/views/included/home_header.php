<!-- BEGIN SIDEBPANEL-->
<nav class="page-sidebar" data-pages="sidebar">
   <!-- START SIDEBAR MENU -->
   <div class="sidebar-menu">
      <!-- BEGIN SIDEBAR MENU ITEMS-->
      <ul class="menu-items">
         <li class="m-t-30 open">
            <a href="<?php echo SITE_PATH; ?>" class="detailed">
               <span class="title">Home</span>
               <span class="details">My Dashboard</span>
            </a>
            <span class="icon-thumbnail bg-success"><i class="pg-home"></i></span>
         </li>
         
            <li class="<?= @$manage_vendors_active ?>">
               <a href="javascript:;">
                  <span class="title">Vendors</span>
                  <span class="arrow open "></span>
               </a>

               <span class="icon-thumbnail "><i class="fa fa-user"></i></span>
               <ul class="sub-menu">
                  <li class="">
                     <a style="cursor:pointer" id="sidebtnToggleSlideUpSize" class="detailed">Add New</a>
                      <span class="icon-thumbnail "><i class="fa fa-user-plus"></i></span>
                  </li>
                  <li class="<?= @$manage_vendors_active ?>">
                     <a href="<?php echo SITE_PATH; ?>vendors">Manage Vendors</a>
                     <span class="icon-thumbnail "><i class="fa fa-list"></i></span>
                  </li>
               </ul>
            </li>


            <li class="<?= @$manage_states_active ?>">
               <a href="javascript:;">
                  <span class="title">States</span>
                  <span class="arrow open "></span>
               </a>

               <span class="icon-thumbnail "><i class="fa fa-map-marker" aria-hidden="true"></i></span>
               <ul class="sub-menu">
                  <li class="active">
                     <a style="cursor:pointer" id="add_new_statebtnToggleSlideUpSize" class="detailed">Add New</a>
                     <span class="icon-thumbnail "><i class="fa fa-plus" aria-hidden="true"></i></span>
                  </li>
                  <li class="<?= @$manage_states_active ?>">
                     <a href="<?php echo SITE_PATH; ?>states">Manage States</a>
                     <span class="icon-thumbnail "><i class="fa fa-list"></i></span>
                  </li>
               </ul>
            </li>
            
            <li class="<?= @$manage_customer_active ?>">
               <a href="javascript:;">
                  <span class="title">Customers</span>
                  <span class="arrow open "></span>
               </a>

               <span class="icon-thumbnail "><i class="fa fa-users" aria-hidden="true"></i></span>
               <ul class="sub-menu">
                  <li class="<?= @$manage_customer_active ?>">
                     <a href="<?php echo SITE_PATH; ?>customer">Manage Customer</a>
                     <span class="icon-thumbnail "><i class="fa fa-list"></i></span>
                  </li>
               </ul>
            </li>
            
            <li class="<?= @$errands_request_active ?>">
               <a href="<?php echo SITE_PATH; ?>jobs">
                  <span class="title">Errand Jobs</span>
               </a>
               <span class="icon-thumbnail "><i class="fa fa-android"></i></span>
            </li>
         
         <li class="">
            <a href="<?php echo SITE_PATH; ?>logout">
               <span class="title">Logout</span>
            </a>
            <span class="icon-thumbnail"><i class="pg-power"></i></span>
         </li>

      </ul>
      <div class="clearfix"></div>
   </div>
   <!-- END SIDEBAR MENU -->
</nav>
<!-- END SIDEBAR -->
<!-- END SIDEBPANEL-->
<!-- START PAGE-CONTAINER -->
<div class="page-container">
   <!-- START HEADER -->
   <div class="header ">
      <!-- START MOBILE CONTROLS -->
      <!-- LEFT SIDE -->
      <div class="pull-left full-height visible-sm visible-xs">
         <!-- START ACTION BAR -->
         <div class="sm-action-bar">
            <a href="#" class="btn-link toggle-sidebar" data-toggle="sidebar">
               <span class="icon-set menu-hambuger"></span>
            </a>
         </div>
         <!-- END ACTION BAR -->
      </div>
      <!-- RIGHT SIDE -->
      <div class="pull-right full-height visible-sm visible-xs">
         <!-- START ACTION BAR -->
         <div class="sm-action-bar">
            <a href="#" class="btn-link" data-toggle="quickview" data-toggle-element="#quickview">
               <span class="icon-set menu-hambuger-plus"></span>
            </a>
         </div>
         <!-- END ACTION BAR -->
      </div>
      <!-- END MOBILE CONTROLS -->
      <div class=" pull-left sm-table">
         <div class="header-inner">
            <div class="brand inline">
               <img src="<?php echo SITE_PATH; ?>assets/images/bq_logo.jpg" alt="logo" data-src="<?php echo SITE_PATH; ?>assets/images/bq_logo.jpg" data-src-retina="<?php echo SITE_PATH; ?>assets/images/bq_logo.jpg" width="78" height="22">
            </div>

         </div>
      </div>
      <div class=" pull-left">
  <?php
    $session = Session::instance();
    if($session->get("demo")){
        echo "<p class='alert alert-warning text-center'>You are on our test environment system. You can play with the platform anyhow !!!</p>";
    }
  ?>
      </div>
      <div class=" pull-right">
         <!-- START User Info-->
         <div class="visible-lg visible-md m-t-10">
            <div class="pull-left p-r-10 p-t-10 fs-16 font-heading"
                 <span class="semi-bold">Welcome, <small class="text-warning"><strong><?php echo $user_type; ?></strong></small></span>
              <span class="text-master"></span>
         </div>
         <div class="dropdown pull-right">
            <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="thumbnail-wrapper d32 circular inline m-t-5">
                <img src="<?php echo SITE_PATH; ?>assets/images/bq_icon.png" alt="" data-src="<?php echo SITE_PATH; ?>assets/images/bq_icon.png" data-src-retina="<?php echo SITE_PATH; ?>assets/images/bq_icon.png" width="32" height="32">
            </span>
            </button>
            <ul class="dropdown-menu profile-dropdown" role="menu">
               <li><a href="<?php echo SITE_PATH; ?>profile"><i class="pg-settings_small"></i> Settings</a>
               </li>
               <li class="bg-master-lighter">
                  <a href="<?php echo SITE_PATH; ?>logout" class="clearfix">
                     <span class="pull-left">Logout</span>
                     <span class="pull-right"><i class="pg-power"></i></span>
                  </a>
               </li>
            </ul>
         </div>
      </div>
      <!-- END User Info-->
   </div>
</div>
<!-- END HEADER -->