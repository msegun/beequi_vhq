<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Profile extends Controller_Vendors {
    
    public function __construct(\Request $request, \Response $response) {
        parent::__construct($request, $response);
    }
    
    public function action_index(){
        $empty_data = json_decode(json_encode(array('data'=>array())), FALSE);
        $notification['status'] = 5;
        $notification['message'] = "";
        $title = "My Profile Management & Config";
        $vendor_id = "";
        
        if($this->request->post('company_update')){
            if(!empty($_FILES['company_logo']['name'])){
                //echo "here";die;
                $logo_path = Utility::uploadVendorImage($_FILES['company_logo'], Text::random('alnum'));
            }
            else{
                $logo_path = $this->request->post('company_logo_url');
            }
            
            $array_to_post = $this->request->post();
            $array_to_post['company_logo_url'] = $logo_path;
            $request = Request::factory(API_CALL_URL.'vendor/self/update/profile/' . $this->access_token.
                    '/'.$this->logged_user,
                    array('strict_redirect' => FALSE, 'follow' => TRUE))
                    ->method(Request::POST)
                    ->post($array_to_post)
                    ->execute();
            $response = json_decode($request->body(), FALSE);
            if($response->status){
                $notification['status'] = 1;
                $notification['message'] = "Basic profile update Operation Successful";
            }
            else{
                $notification['status'] = 0;
                $notification['message'] = "Basic profile update Operation Failed";
            }            
        }
        //load user profile
        $curl_response = Model::factory('Curl')->get(API_CALL_URL.'vendor/self/view/'.$this->access_token.'/'.$this->logged_user);
        $curl_response_basic = json_decode($curl_response);
        $curl_response = Model::factory('Curl')->get(API_CALL_URL.'vendor/self/view/state_of_operations/'.$this->access_token.'/'.$this->logged_user);
        $curl_response_state_of_operations = json_decode($curl_response);
        $curl_response = Model::factory('Curl')->get(API_CALL_URL.'vendor/view/logs/'.$this->access_token.'/'.$vendor_id);
        $curl_response_logs = json_decode($curl_response);
        $curl_response = Model::factory('Curl')->get(API_CALL_URL.'states/list/#');
        $curl_response_states_list = json_decode($curl_response, FALSE);
        
        $view = $this->render_profile($title, $notification, $curl_response_basic, $curl_response_state_of_operations,
            $curl_response_logs, $curl_response_states_list, $vendor_id, "our_profile_active", false);
        $this->response->body($view);
    }
    
}
