<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="<?php echo SITE_PATH; ?>pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo SITE_PATH; ?>pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo SITE_PATH; ?>pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo SITE_PATH; ?>pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="BEE QUI RESOURCES ADMIN DASHBOARD" name="description" />
    <meta content="BEE QUI RESOURCES" name="author" />
    <link href="<?php echo SITE_PATH; ?>assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_PATH; ?>assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_PATH; ?>assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_PATH; ?>assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_PATH; ?>assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo SITE_PATH; ?>assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo SITE_PATH; ?>assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo SITE_PATH; ?>assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_PATH; ?>assets/plugins/bootstrap-tag/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_PATH; ?>assets/plugins/dropzone/css/dropzone.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_PATH; ?>assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo SITE_PATH; ?>assets/plugins/summernote/css/summernote.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo SITE_PATH; ?>assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo SITE_PATH; ?>assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo SITE_PATH; ?>pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo SITE_PATH; ?>assets/plugins/jquery-datatable/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_PATH; ?>assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_PATH; ?>assets/plugins/datatables-responsive/css/datatables.responsive.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo SITE_PATH; ?>assets/plugins/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo SITE_PATH; ?>assets/plugins/bootgrid/jquery.bootgrid.min.css" rel="stylesheet" type="text/css"/>
    <link class="main-stylesheet" href="<?php echo SITE_PATH; ?>pages/css/pages.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
        <link href="<?php echo SITE_PATH; ?>pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <!--[if lt IE 9]>
            <link href="<?php echo SITE_PATH; ?>assets/plugins/mapplic/css/mapplic-ie.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script src="<?php echo SITE_PATH; ?>assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
    }
    </script>
  </head>
  
  <body class="fixed-header  dashboard ">

    <?php echo $home_header_content; ?>
      
      <div class="page-content-wrapper">

            <?php echo $dynamic_view; ?>

        <!-- END PAGE CONTENT -->
        <!-- START COPYRIGHT -->
        <!-- START CONTAINER FLUID -->
        <?php 
        echo $footer; 
        echo $create_user_modal_content; 
        echo $create_state_modal_content;
        ?>
        <?= @$other_model ?>
        <!-- END COPYRIGHT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTAINER -->
    
   

    <!-- END OVERLAY -->
    <!-- BEGIN VENDOR JS -->
    <script src="<?php echo SITE_PATH; ?>assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="<?php echo SITE_PATH; ?>assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script type="text/javascript" src="<?php echo SITE_PATH; ?>assets/plugins/classie/classie.js"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/jquery-metrojs/MetroJs.min.js" type="text/javascript"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/jquery-sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/skycons/skycons.js" type="text/javascript"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/moment/moment.min.js"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo SITE_PATH; ?>assets/plugins/jquery-autonumeric/autoNumeric.js"></script>
    <script type="text/javascript" src="<?php echo SITE_PATH; ?>assets/plugins/dropzone/dropzone.min.js"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js" type="text/javascript"></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/datatables-responsive/js/datatables.responsive.js" type="text/javascript" ></script>
    <script src="<?php echo SITE_PATH; ?>assets/plugins/jquery-dynatree/jquery.dynatree.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo SITE_PATH; ?>assets/plugins/datatables-responsive/js/lodash.min.js"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="<?php echo SITE_PATH; ?>pages/js/pages.min.js"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="<?php echo SITE_PATH; ?>assets/plugins/bootgrid/jquery.bootgrid.updated.js" type="text/javascript"></script>
    <script src="<?php echo SITE_PATH; ?>assets/js/form_elements.js" type="text/javascript"></script>
    <script src="<?php echo SITE_PATH; ?>assets/js/datatables.js" type="text/javascript"></script>
    <script src="<?php echo SITE_PATH; ?>assets/js/demo.js" type="text/javascript"></script>
    <script src="<?php echo SITE_PATH; ?>assets/js/scripts.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
  </body>
</html>