        <!-- START PAGE CONTENT -->
        <div class="content sm-gutter">
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid padding-25 sm-padding-10">

<?php
if(isset($vendor_details->active)){
?>
<!-- START PANEL -->
<div class="panel panel-transparent">
   <div class="panel-body">
       <?php
        try{
        $rating = (intval(@$vendor_details->rating->total_points)/intval(@$vendors->rating->raters_count))*5;
        }
        catch(ErrorException $e){ $rating = "<strong class='text-warning'>Unrated</strong>"; }
       ?>
      <h3><?php if($is_admin){?><a href="<?= SITE_PATH ?>vendors"><i class="fa fa-backward"></i></a> <?php } ?>
          -<strong class="text-<?php if($vendor_details->active){echo 'success';}else{echo 'danger';} ?>">
          <?php echo $vendor_details->vendor_name; ?></strong>&nbsp;<small>( <?= $rating ?> )</small>
        <a href="<?= SITE_PATH ?><?php if($is_admin){ ?>vendors/<?php }?>profile/<?= $vendor_id ?>"><i class="fa fa-home"></i></a>
      </h3>

      <?php
      if($notification['status'] == 0){
         echo'<div class="alert alert-danger" role="alert">
                      <button class="close" data-dismiss="alert"></button>
               '.$notification['message'].'
             </div>
              ';

      }elseif($notification['status'] == 1){
         echo'<div class="alert alert-success" role="alert">
                      <button class="close" data-dismiss="alert"></button>
               '.$notification['message'].'
             </div>
              ';
      }else{

      }
      ?>
      
      <div class="panel panel-success col-sm-6">
          <div class="panel-heading">Basic Profile</div>
          <div class="panel-body row">
              <div class="col-sm-6"><strong>Vendor Name</strong></div><div class="col-sm-6"><?php echo @$vendor_details->vendor_name ?></div>
              <div class="col-sm-6"><strong>Login Email</strong></div><div class="col-sm-6"><?php echo @$vendor_details->login_credentials->email ?></div>
              <div class="col-sm-6"><strong>Company Name</strong></div><div class="col-sm-6">
                  <?php if(isset($vendor_details->profile->company_name)){ echo $vendor_details->profile->company_name;}else{echo "?";} ?></div>
              <div class="col-sm-6"><strong>Address</strong></div><div class="col-sm-6">
                  <?php if(isset($vendor_details->profile->company_address)){ echo $vendor_details->profile->company_address;}else{echo "?";} ?></div>
              <div class="col-sm-6"><strong>City</strong></div><div class="col-sm-6">
                  <?php if(isset($vendor_details->profile->company_city)){echo $vendor_details->profile->company_city;}else{echo "?";} ?></div>
              <div class="col-sm-6"><strong>State</strong></div><div class="col-sm-6">
                  <?php if(isset($vendor_details->profile->company_state)){echo $vendor_details->profile->company_state;}else{echo "?";} ?></div>
              <div class="col-sm-6"><strong>Telephone</strong></div><div class="col-sm-6">
                  <?php if(isset($vendor_details->profile->company_telephone)){echo $vendor_details->profile->company_telephone;}else{echo "?";} ?></div>
              <div class="col-sm-6"><strong>Website</strong></div><div class="col-sm-6">
                  <?php if(isset($vendor_details->profile->company_website)){echo $vendor_details->profile->company_website;}else{echo "?";} ?></div>
              <div class="col-sm-6"><strong>Slogan</strong></div><div class="col-sm-6">
                  <?php if(isset($vendor_details->profile->company_slogan)){echo $vendor_details->profile->company_slogan;}else{echo "?";} ?></div>
              <div class="col-sm-6"><strong>Created</strong></div><div class="col-sm-6"><?php echo $vendor_details->created ?></div>
              <div class="col-sm-6"><strong>Logo URL</strong></div><div class="col-sm-6 sm"><small>
                  <?php if(isset($vendor_details->profile->company_logo_url)){echo $vendor_details->profile->company_logo_url;}else{echo "?";} ?>
                  </small></div>
              <div class="col-sm-2"></div>
              <div class="col-sm-10 text-right">
                  <?php if(!$is_admin){ ?>
                  <small class="alert alert-danger col-sm-7">Your account will be temporarily suspended on profile update</small>
                  <?php  } ?>
                  <button class="btn btn-primary btn-sm" data-target="#editProfile" data-toggle="modal">Edit / Complete Profile</button>
                  <?php 
                  if($vendor_details->active){
                      $change = 'false';
                      $btn_lang = "Disable";
                      $btn_color = "danger";
                  }else{
                      $change = 'true';
                      $btn_lang = "Enable";
                      $btn_color = "success";
                  } 
                  if($is_admin){
                  ?>
                  <a href="<?= SITE_PATH ?>vendors/status/<?= $vendor_id ?>/<?= $change ?>" class="btn btn-<?= $btn_color ?> btn-sm">
                      <?= $btn_lang ?></a>
                    <?php } ?>
              </div>
          </div>
      </div>
      <div class="panel panel-info col-sm-6" style="height: 322px;overflow: auto;">
          <div class="panel-heading">States of operation : 
              &nbsp;&nbsp;<?php if($is_admin){?><a href="#" id="add_state_ModalBtn" title="Add State">
              <i class="fa fa-plus-circle" aria-hidden="true"></i></a> <?php } ?>
          </div>
          <div class="panel-body row">
              <?php if(!$is_admin){ ?>
              <div class='alert alert-info' role="alert">
                  <button class="close" data-dismiss="alert"></button>
                  Your "States of Operation" are regularly reviewed after you have provided and complied to all necessary KYC.
                  Failure to perform in any of the states below will attract account suspension. To add or Remove any state, 
                  Do call or send us an email. Thank you.
              </div>
              <?php } ?>
                <div id="states_tree" class="m-b-20 col-sm-6">
                    <ul>
                        <?php
                        $found = array();
                          foreach($vendor_state_of_operations->states_of_operation as $value){
                              $_state_name = "";
                              $_state_cities = array();
                              if(in_array($value->state_id, $found)){
                                  continue;
                              }
                              $found[$value->state_id] = $value->state_id;
                              foreach($states_list as $state){
                                  if($state->_id == $value->state_id){
                                      $_state_name = $state->name;
                                      $_state_cities = $state->cities;
                                  }
                              }
                        ?>
                              <li id="<?= $_state_name ?>" class="folder"> <?= $_state_name ?>
                                  <ul>
                                      <?php foreach($_state_cities as $cities) {?>
                                      <li id="<?= $_state_name.".".$cities ?>"><?= $cities ?></li>
                                      <?php } ?>
                                  </ul>
                              </li>
                        <?php 
                          }
                        ?>
                    </ul>
                </div>
              <div class="col-sm-6">
                  <?php if($is_admin){ ?>
                <form method="post" id="remove_state_form">
                    <select class="" name="state_to_remove">
                        <option value="">Select State</option>
                        <?php
                        $allStatesOperation = array();
                        $loop = 0;
                        foreach($vendor_state_of_operations->states_of_operation as $each){
                            $allStatesOperation[$loop] = $each->state_id;
                            $loop++;
                        }
                        foreach($states_list as $state){
                            if(in_array($state->_id, $allStatesOperation)){
                                echo "<option value='".$state->_id."'>".$state->name."</option>";
                            }
                        }
                        ?>
                    </select>
                    <input type="hidden" name="remove_state" value="yes" />
                &nbsp;&nbsp;<a href="#" onclick="$('#remove_state_form').submit();" id="add_state_ModalBtn" title="Remove State">
                    <i class="fa fa-minus-circle text-danger" aria-hidden="true"></i></a>
                </form>
                  <?php } ?>
                  <br /><br />
                  <div class="panel" style="width:150px; height: 150px;">
                      <div class="panel-body" >
                          <img src="<?php if(strlen(@$vendor_details->profile->company_logo_url) > 4) { echo SITE_PATH.$vendor_details->profile->company_logo_url;}
                  else{echo SITE_PATH.'assets/images/bq_icon.png';}?>" style="width:90px; height: 90px;" class="img img-responsive img-rounded"/>
                  
                      </div>
                  </div>
               </div>
          </div>
      </div>
       <?php if($is_admin){ ?>
       <div class="panel panel-danger col-sm-12">
          <div class="panel-heading">
              Password Reset :
          </div>
           <div class="panel-body row">
               <form role="form" method="post" class="form-inline">
                  <div class="row">
                    <div class="col-sm-5">
                      <div class="form-group form-group-default required">
                        <label>New Password</label>
                        <input type="password" name="false_password" class="form-control hidden">
                        <input type="password" name="false_password2" class="form-control hidden">
                        <input type="password" name="password" class="form-control" autocomplete="off" required>
                      </div>
                    </div>
                    <div class="col-sm-5">
                      <div class="form-group form-group-default" required>
                        <label>Type Password Again</label>
                        <input type="password" name="password_again" autocomplete="off" class="form-control" required>
                      </div>
                    </div>
                    <div class="col-sm-2">
                          <input type="submit" class="btn btn-primary" name="reset_password" value="Reset Password">
                    </div>
                  </div>
               </form>
           </div>
       </div>
       <?php } ?>
       <div class="panel panel-success col-sm-12">
          <div class="panel-heading">
              Field Agents Provisioning : &nbsp;<a href="" id="add_agent_ModalBtn" title="Add Agents">
                  <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
          </div>
           <div class="panel-body row">
           </div>
       </div>
      
   </div>
</div>
<!-- END PANEL -->







<!-- MODAL STICK UP  -->
<div class="modal fade stick-up" id="add_state_Modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header clearfix text-left">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
        </button>
        <h5>Add State of <span class="semi-bold">Operation</span></h5>
        <p>Select state from dropdown box then Add.</p>
      </div>
      <div class="modal-body">
        <form role="form" method="post" action="">
          <div class="form-group-attached">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group form-group-default">
                  <label>List of Supported States</label>
                  <select class="form-control" name="state_to_add">
                      <option value="">Select a state</option>
                      <?php
                      foreach($states_list as $state){
                          echo "<option value='".$state->_id."'>".$state->name."</option>";
                      }
                      ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
            <div class="row text-center m-t-10">
                <input type="submit" name="add_state" value="Add a State" class="btn btn-primary"/>
            </div>
        </form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- END MODAL STICK UP  -->




<!-- MODAL STICK UP SMALL ALERT -->
<div class="modal fade slide-right" id="editProfile" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content-wrapper">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
        </button>
        <div class="container-xs-height full-height">
          <div class="row-xs-height">
            <div class="modal-body col-xs-height col-middle">
              <form method="post" action="" enctype="multipart/form-data">
                  <div class="form-group">
                    <label>Company Name</label>
                    <input type="text" name="company_name" class="form-control" value="<?= @$vendor_details->profile->company_name ?>" required>
                  </div>
                  <div class="form-group">
                    <label>Company Address</label>
                    <textarea class="form-control" name="company_address"><?= @$vendor_details->profile->company_address ?></textarea>
                  </div>
                  <div class="form-group">
                    <label>Company City</label>
                    <input type="text" name="company_city" class="form-control" value="<?= @$vendor_details->profile->company_city ?>" required>
                  </div>
                  <div class="form-group">
                    <label>State</label>
                    <select class="form-control" name="company_state" required>
                        <option value="">Select a state</option>
                        <?php
                        foreach($states_list as $state){
                            $selected = "";
                            if($state->name == @$vendor_details->profile->company_state){
                                $selected = "selected";
                            }
                            echo "<option value='".$state->name."' ".$selected.">".$state->name."</option>";
                        }
                        ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Telephone Number</label>
                    <input type="text" name="company_telephone" class="form-control" value="<?= @$vendor_details->profile->company_telephone ?>" required>
                  </div>
                  <div class="form-group">
                    <label>Company Website</label>
                    <input type="text" name="company_website" class="form-control" value="<?= @$vendor_details->profile->company_website ?>" required>
                  </div>
                  <div class="form-group">
                    <label>Company Slogan</label>
                    <input type="text" name="company_slogan" class="form-control" value="<?= @$vendor_details->profile->company_slogan ?>" required>
                  </div>
                  <div class="form-group">
                    <label>Company Logo</label>
                    <input type="hidden" name="company_logo_url" value="<?= @$vendor_details->profile->company_logo_url ?>" />
                    <input type="file" name="company_logo" class="form-control">
                  </div>
                  <input type="submit" class="btn btn-primary btn-block" name="company_update" value="Save" />
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- END MODAL STICK UP SMALL ALERT -->
<?php
}
else{
?>
<div class="alert alert-danger text-center">Something is wrong somewhere, can't open profile at this time.</div>
<?php
}
?>

          </div>
          <!-- END CONTAINER FLUID -->
        </div>



