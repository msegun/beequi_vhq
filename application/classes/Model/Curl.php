<?php defined('SYSPATH') or die('No direct script access.');

class Model_Curl extends Model
{

 function get($url){
  try{
   $request = Request::factory($url,
       array('strict_redirect' => FALSE, 'follow' => TRUE))
       ->execute();
   return $request->body();
  }
  catch(Kohana_Exception $e){
   $log = Log::instance();
   $log->add(Log::NOTICE, "ShortenUrl didnt work: ".$e->getMessage());
   return $e;
  }

 }
 

}