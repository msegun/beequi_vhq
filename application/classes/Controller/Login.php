<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Login extends Controller {
	
	public function action_index()
	{
		$response['message'] = '';
		$response['success'] = 3;

		if(isset($_SESSION['kudiuser']) || isset($_COOKIE['kudiuser'])){

			if($_COOKIE['kudiuser'] == ""){
				header("location: " . SITE_PATH . "logout");
				echo '<script type="text/javascript">document.location = "'.SITE_PATH.'logout";</script>';
				exit;

			}else {

				Model::factory('User')->set_session($_COOKIE['kudiuser'], $_COOKIE['kudipass']);

				header("location: " . SITE_PATH . "home");
				echo '<script type="text/javascript">document.location = "'.SITE_PATH.'home";</script>';
				exit;
			}
		}

		if(isset($_REQUEST['isLogin'])){
			$email = Model::factory('Filter')->filter_data($_REQUEST['username']);
			$password = Model::factory('Filter')->filter_data($_REQUEST['password']);

			if(isset($_REQUEST['remember'])){
				$remember = 1;
			}else{
				$remember = 0;
			}

			$response = Model::factory('User')->login($email, $password, $remember);
		}

		if($response['success'] == 1){
			header("location: ".SITE_PATH."home");
			echo '<script type="text/javascript">document.location = "'.SITE_PATH.'home";</script>';
			exit;
		}

		$page_title = "Welcome To Kudimoney";

		$signup_response_code = 3;
		$signup_response_mess = '';

		$this->view = View::factory('template/index.tpl')
			->bind('page_title', $page_title)
			->bind('signup_response_message', $signup_response_code)
			->bind('signup_response_code', $signup_response_mess)
			->bind('login_response_message', $response['message'])
			->bind('login_response_code', $response['success']);

		$this->response->body($this->view); //will comment this later
		 
	
	}       
} // End Welcome
