<!-- START PANEL -->
<div class="panel panel-transparent">
   <div class="panel-body">

       <h3><?= ucfirst($task) ?> Jobs:</h3>
        <div class="panel-body">

<table id="data-table-pending" class="table table-striped table-vmiddle">
    <thead>
        <tr>
            <th data-column-id="date">TIME</th>
<!--            <th data-column-id="batch">BATCH</th>-->
            <th data-column-id="ref" data-sortable="false">REF</th>
<!--            <th data-column-id="source">SOURCE</th>-->
            <th data-column-id="from" data-sortable="false">FROM</th>
            <th data-column-id="to">TO</th>
<!--            <th data-column-id="weight" data-sortable="false">WEIGHT</th>-->
            <th data-column-id="commands" data-formatter="commands" data-sortable="false">Action</th>
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>
        </div>

   </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        
        var grid_services = $("#data-table-pending").bootgrid({
            ajax: true,
            ajaxSettings: {
                method: "POST",
                cache: false
            },

            labels: {
                search: "Batch or REF"
            },
            url: "<?= SITE_PATH ?>tables/transaction/<?= $task ?>",
            formatters: {
                "commands": function(column, row)
                {
                    return "<button type=\"button\" class=\"btn btn-xs btn-default command-view\" data-row-id=\"" + 
                row.id + "\" title=\"See More Info\"><span class=\"zmdi zmdi-eye\"></span></button>";
                }
            }
        }).on("loaded.rs.jquery.bootgrid", function()
        {
            /* Executes after data is loaded and rendered */
            grid_services.find(".command-view").on("click", function(e)
            {
                location.href="<?= SITE_PATH ?>jobs/view/"+$(this).data("row-id")
            });
        });
    });
</script>


