<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller {

    private $session;
    
    public function __construct(\Request $request, \Response $response) {
        parent::__construct($request, $response);
        $this->session = Session::instance();
    }

    public function action_index()
    {
        $this->session->delete("demo");
            if($this->session->get("admin_access_token")){
                HTTP::redirect(SITE_PATH.'home');
            }

            $loginresponse['status'] = 3;
            $loginresponse['message'] = '';

            $page_title = "Admin Login";

            if($this->request->post('isLog')){

                    $response = array();

                    $user_id = $this->request->post("log_user_id");
                    $password = $this->request->post("log_pass");
                    $log_as = intval($this->request->post("log_as"));

                    if(empty($user_id) || empty($password)){
                             $loginresponse['status'] = 0;
                             $loginresponse['message'] = "Empty filed(s)";
                    }else{
                        if($log_as == 1){
                            $curl_response = Model::factory('Curl')->get(API_CALL_URL.'auth/'.$user_id.'/'.$password);
                        }
                        else{
                            $curl_response = Model::factory('Curl')->get(API_CALL_URL.'vendor/auth/'.$user_id.'/'.$password);
                        }
                        $curl_response = json_decode($curl_response);
                        $loginresponse['status'] = $curl_response->status;
                        $loginresponse['message'] = $curl_response->data;

                        if($loginresponse['status']){
                            $this->session->set('admin_access_token', $loginresponse['message']->token);
                            $this->session->set('user_id', $user_id);
                            $this->session->set('identity', $log_as);
                            $this->session->set('db_id', $curl_response->data->_id);
                            HTTP::redirect(SITE_PATH.'home');
                        }

                    }
            }


            $this->view = View::factory('template/index.tpl')
                              ->bind('login_response', $loginresponse)
                          ->bind('page_title', $page_title);

            $this->response->body($this->view); //will comment this later
    }

    public function action_demo()
    {
        $this->session->set("demo", true);
        
            if($this->session->get("admin_access_token")){
                HTTP::redirect(SITE_PATH.'home');
            }

            $loginresponse['status'] = 3;
            $loginresponse['message'] = '';

            $page_title = "Admin Login";

            if($this->request->post('isLog')){

                    $response = array();

                    $user_id = $this->request->post("log_user_id");
                    $password = $this->request->post("log_pass");
                    $log_as = intval($this->request->post("log_as"));

                    if(empty($user_id) || empty($password)){
                             $loginresponse['status'] = 0;
                             $loginresponse['message'] = "Empty filed(s)";
                    }else{
                        if($log_as == 1){
                            $curl_response = Model::factory('Curl')->get(API_CALL_URL.'auth/'.$user_id.'/'.$password);
                        }
                        else{
                            $curl_response = Model::factory('Curl')->get(API_CALL_URL.'vendor/auth/'.$user_id.'/'.$password);
                        }
                        $curl_response = json_decode($curl_response);
                        $loginresponse['status'] = $curl_response->status;
                        $loginresponse['message'] = $curl_response->data;

                        if($loginresponse['status']){
                            $this->session->set('admin_access_token', $loginresponse['message']->token);
                            $this->session->set('user_id', $user_id);
                            $this->session->set('identity', $log_as);
                            $this->session->set('db_id', $curl_response->data->_id);
                            HTTP::redirect(SITE_PATH.'home');
                        }

                    }
            }


            $this->view = View::factory('template/index.tpl')
                              ->bind('login_response', $loginresponse)
                          ->bind('page_title', $page_title);

            $this->response->body($this->view); //will comment this later
    }
    
    public function action_mobile(){
            $this->view = View::factory('template/mobile');
            $this->response->body($this->view); //will comment this later
    }

} // End Welcome
