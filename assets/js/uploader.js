var uploader = new plupload.Uploader({
	runtimes : 'html5,html4',
	browse_button : 'pickfiles', // you can pass an id...
	container: document.getElementById('container'), // ... or DOM Element itself
	url : $('#root_path').val()+'slide/uploader',
	prevent_duplicates: true,
	
	filters : {
		max_file_size : '15mb',
		mime_types: [
			{title : "Image files", extensions : "jpg,jpeg,gif,png"}
		]
	},
	
	multipart_params : {
        "slide_id" : $('#slide_id').val(),
    },
	
	// Resize images on clientside if we can
        resize: {
            quality : 60
        },
	init: {
		PostInit: function() {
			document.getElementById('filelist').innerHTML = '';
			document.getElementById('uploadfiles').onclick = function() {
				uploader.start();
				return false;
			};
		},
		FilesAdded: function(up, files) {
			if(up.files.length > $('.photo_limit').val() )
                     {
		$.each(files, function(i, file) {
            up.removeFile(file);    
        });
                        up.splice($('.photo_limit').val());
                        alert('no more than '+$('.photo_limit').val() + ' file(s)');
                     }else{
			plupload.each(files, function(file) {
				
				if(file.type == "image/jpg" || file.type == "image/jpeg" ){ 
				file_icon = '<img src="images/jpg.png" align="absmiddle" border="0" alt="" />'; 
				}else if( file.type == "png" ){ file_icon = '<img src="images/png.png" align="absmiddle" border="0" alt="" />'; }
				else if( file.type == "gif" ){ file_icon = '<img src="images/gif.png" align="absmiddle" border="0" alt="" />'; }
				else {  file_icon = '<img src="images/unknown.png" align="absmiddle" border="0" alt="" />'; }
				
				document.getElementById('filelist').innerHTML += '<div id="' + file.id + '" class="bx_main"> <div class="bx_1">'+file_icon+'</div><div class="bx_2"><span class="fname">' + file.name.substring(0, 30) + '</span> <br/> <span class="fsize">'+ plupload.formatSize(file.size)+'</span> <span style="float:right; margin-right:5px;" id="ferr_'+file.id+'"></span>  </div><div class="bx_3" id="bx_3_'+file.id+'"> <a style="cursor:pointer" onclick="remove_this_file(\''+file.id+'\');"> <img border="0" src="images/ico_cancel.png" width="16" height="15"> <b></b> </div> </div>';
			});
					 }
		},
		BeforeUpload: function(up, file) {
			$(".please_wait").show();
			$("#bx_3_"+file.id).html('<img src="images/loader.gif" width="16" height="16" align="absmiddle" title="File Uploading"/>');
		},
		
		UploadProgress: function(up, file) {
			$("#bx_3_"+file.id).html('<img src="images/loader.gif" width="16" height="16" align="absmiddle" title="File Uploading"/>');
		},
		FileUploaded: function(up, file, response) {
			response = jQuery.parseJSON( response.response );
			
			if(response.error.code == 1){
			$("#bx_3_"+file.id).html('<img src="images/ico_ok.png" width="16" height="16" align="absmiddle" title="Upload Completed"/>');
			}else{
			$("#bx_3_"+file.id).html('<img src="images/cancel.png" width="16" height="16" align="absmiddle" title="'+response.error.message+'"/>');	
			}
			
			$('#ferr_'+file.id).html(response.error.message);
		},
		UploadComplete: function(up, file) {
			$.fn.uploadCompleted();
		},
		Error: function(up, err) {
			$("#bx_3_"+err.file.id).html('<img src="images/cancel.png" width="16" height="16" align="absmiddle" title="'+err.message+'"/>');
			document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
		}
	}
});
uploader.init();


function remove_this_file(id)
{
	if(confirm('Are you sure to remove this file?'))	{
		$("#removed_files").append('<input type="hidden" id="'+id+'" value="'+id+'">');
		$("#"+id).hide();
		$("#"+id).attr("src",blank);
	}
	return false;
}