<?php
$current_action = Request::current()->action();
if($current_action == 'pending'){
    $pending_jobs_active = 'active';
}
else if($current_action == 'processing'){
    $processing_jobs_active = 'active';
}
else if($current_action == 'completed'){
    $completed_jobs_active = 'active';
}
?>

<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper full-height">
    <!-- START PAGE CONTENT -->
    <div class="content full-height">
      <!-- START EMAIL -->
      <div class="email-wrapper">
        <!-- START EMAIL SIDEBAR MENU-->
        <nav class="email-sidebar padding-30">
          <button class="btn btn-complete btn-block btn-compose m-b-30 disabled">New Job</button>
          <p class="menu-title">Errand Jobs</p>
          <ul class="main-menu">
            <li class="<?= @$pending_jobs_active ?>">
              <a href="<?php echo SITE_PATH; ?>jobs/pending">
                <span class="title"><i class="pg-inbox"></i> Pending</span>
                <span class="badge pull-right"><?= $pending_jobs ?></span>
              </a>
            </li>
            <li class="<?= @$processing_jobs_active ?>">
              <a href="<?php echo SITE_PATH; ?>jobs/processing">
                <span class="title"><i class="pg-folder"></i> Processing</span>
                <span class="badge pull-right"><?= $processing_jobs ?></span>
              </a>
            </li>
            <li class="<?= @$completed_jobs_active ?>">
              <a href="<?php echo SITE_PATH; ?>jobs/completed">
                <span class="title"><i class="pg-sent"></i> Completed</span>
                <span class="badge pull-right"><?= $completed_jobs ?></span>
              </a>
            </li>
            <li class="<?= @$junks_active ?>">
              <a href="<?php echo SITE_PATH; ?>jobs/junks">
                <span class="title"><i class="pg-spam"></i> Junk Yard</span>
                <span class="badge pull-right"><?= $junks ?></span>
              </a>
            </li>
          </ul>
          <p class="menu-title m-t-20 all-caps">Customer Review</p>
          <ul class="sub-menu no-padding">
            <li>
              <a href="#">
                <span class="title">Rating Status</span>
                <span class="badge pull-right"></span>
              </a>
            </li>
<!--            <li>
              <a href="#">
                <span class="title">Reports</span>
                <span class="badge pull-right">5</span>
              </a>
            </li>-->
          </ul>
        </nav>
        <!-- END EMAL SIDEBAR MENU -->
        <!-- START COMPOSE EMAIL -->
        <div class="email-composer container-fluid">
            <?= $sub_view ?>
        </div>
        <!-- END COMPOSE EMAIL -->
      </div>
      <!-- END EMAIL -->
    </div>
    <!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTENT WRAPPER -->
