<?php
if($can_display){
?>
<div class="panel panel-transparent">
  <div class="panel-heading">
    <div class="panel-title">
        <h3>Job #<strong class="text-info"><?= $transaction->transaction_batch_id ?></strong>  
            REF #<strong class="text-info"><?= $transaction->transaction_ref ?></strong> 
            (<span class="text-primary"><?= $transaction->errand->errand_status ?></span>):</h3>
    </div>
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-transparent">
        Payment: <code class="text-<?= @$payment_status_type; ?>"><?= $transaction->payment->payment_status; ?></code>
        <br>
          <!-- Nav tabs -->
          <?php
          if($alert_type != ""){
          ?>
          <p class="text-center alert m-t-10 alert-<?= $alert_type ?>"><?= $alert_message ?></p>
          <?php
          }
          ?>
          <ul class="nav nav-tabs nav-tabs-linetriangle">
            <li class="<?= $tabs['brief'] ?>">
              <a data-toggle="tab" href="#brief"><span>Job Request Brief</span></a>
            </li>
            <li class="<?= $tabs['response'] ?>">
              <a data-toggle="tab" href="#response"><span class="text-warning-dark">Respond Here</span></a>
            </li>
            <li class="<?= $tabs['actions'] ?>">
              <a data-toggle="tab" href="#actions"><span>Actions Audit</span></a>
            </li>
            <li class="<?= $tabs['payment'] ?>">
                <a data-toggle="tab" href="#payment"><span class="text-success">Payment</span></a>
            </li>
          </ul>
          <!-- Tab panes -->
          <div class="tab-content">
            <div class="tab-pane fade in <?= $tabs['brief'] ?>" id="brief">
              <div class="row column-seperation">
                  <div class="col-sm-6">
                    <ul class="bold">
                      <li>
                        <code>From State</code> <span class="normal"><?= $transaction->query->state_origin ?></span>
                      </li>
                      <li>
                        <code>From Region</code> <span class="normal"><?= $transaction->query->region_origin ?></span>
                      </li>
                      <li>
                        <code>From Full Address</code> <span class="normal"><?= $transaction->errand->origin_full_address ?></span>
                      </li>
                      <li>
                        <code>From Contact Phone</code> <span class="normal"><?= $transaction->query->region_destination ?></span>
                      </li>
                      <li>
                        <code>To State</code> <span class="normal"><?= $transaction->query->state_destination ?></span>
                      </li>
                      <li>
                        <code>To Region</code> <span class="normal"><?= $transaction->query->region_destination ?></span>
                      </li>
                      <li>
                        <code>To Full Address</code> <span class="normal"><?= $transaction->errand->destination_full_address ?></span>
                      </li>
                      <li>
                        <code>To Contact Phone</code> <span class="normal"><?php 
                        if($is_admin){echo $transaction->errand->origin_contact_number;}
                        else{if($transaction->errand->errand_status == "PENDING"){echo 'WITHHELD';}else{echo $transaction->errand->origin_contact_number;}} ?></span>
                      </li>
                    </ul>
                  </div>
                  <div class="col-sm-6">
                    <ul class="bold">
                      <li>
                        <code>Customer name</code> <span class="normal"><?php 
                        if($is_admin){echo $transaction->errand->origin_contact_number;}
                        else{
                        if($transaction->errand->errand_status == "PENDING"){echo 'WITHHELD';}else{echo $transaction->errand->customer_id->customer_fullname;}} ?></span>
                      </li>
                      <li>
                        <code>Content Description</code> <span class="normal"><?= $transaction->errand->content_description ?></span>
                      </li>
                      <li>
                        <code>Weight</code> <span class="normal"><?= $transaction->errand->item_weight ?></span>
                      </li>
                      <li>
                        <code>Cash handling</code> <span class="normal"><?= $transaction->errand->cash_handle ?></span>
                      </li>
                      <li>
                        <code>Additional Description</code> <span class="normal"><?= $transaction->errand->additional_instruction ?></span>
                      </li>
                      <li>
                        <code>TimeStamp Initiated</code> <span class="normal"><?= @$transaction->errand->errand_created ?></span>
                      </li>
                      <li>
                        <code>Initiated Source</code> <span class="normal"><?= $transaction->source ?></span>
                      </li>
                    </ul>
                  </div>
              </div>
            </div>
            <div class="tab-pane fade in <?= $tabs['response'] ?>" id="response">
              <div class="row">
                <div class="col-md-12">

            <div class="modal-header clearfix text-left">
              <h5>Billing information</h5>
            </div>
            <div class="modal-body">
                <form role="form" action="" method="post">
                <div class="form-group-attached">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group form-group-default">
                        <label>Errand cost (&#8358)</label>
                        <input type="text" name="errand_cost" id="errand_cost" class="form-control key_press" 
                               value="<?= $form_data['errand_cost'] ?>" 
                               <?php if($transaction->errand->errand_status != "PENDING" || !$response_editable){echo 'readonly';} ?> required>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group form-group-default">
                          <label>Cash handling fee on <strong><?= $transaction->errand->cash_handle ?></strong> (&#8358)</label>
                        <input type="text" name="cash_handling_cost" id="cash_handling_cost" class="form-control key_press" 
                               value="<?= $form_data['cash_handling_cost'] ?>" required
                               <?php if($transaction->errand->cash_handle == "Don't Worry" || $transaction->errand->errand_status != "PENDING" 
                                       || !$response_editable){echo 'readonly';} ?>>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group form-group-default">
                        <label>Tax (%)</label>
                        <input type="text" name="tax" id="tax" class="form-control key_press" value="0" 
                               <?php if($transaction->errand->errand_status != "PENDING" || !$response_editable){echo 'readonly';} ?> 
                               <?= $form_data['tax'] ?> required>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group form-group-default">
                        <label>Proposed Pick up Day</label>
                        <div id="datepicker-component" class="input-group date key_press">
                            <input type="text" name="pickup_date" class="form-control" 
                                   <?php if($transaction->errand->errand_status != "PENDING" || !$response_editable){echo 'disabled';} ?> 
                                   value="<?= $form_data['pickup_date'] ?>" required><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group form-group-default">
                        <label>Proposed Delivery Day</label>
                        <div id="datepicker-component" class="input-group date key_press">
                            <input type="text" name="delivery_date" class="form-control" 
                                   <?php if($transaction->errand->errand_status != "PENDING" || !$response_editable){echo 'disabled';} ?> 
                                   value="<?= $form_data['delivery_date'] ?>" required><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group form-group-default">
                          <label>What we pay you on settlement(&#8358)</label>
                          <input type="text" name="what_we_pay_you" id="what_we_pay_you" class="form-control" 
                                 value="<?= $form_data['what_we_pay_you'] ?>" readonly>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group form-group-default">
                          <label>Our commission (10%) (&#8358)</label>
                          <input type="text" name="our_commission" id="our_commission" class="form-control" 
                                 value="<?= $form_data['our_commission'] ?>" readonly>
                      </div>
                    </div>
                  </div>
                </div>
              
                <div class="row">
                  <div class="col-sm-8">
                    <div class="p-t-20 clearfix p-l-10 p-r-10">
                      <div class="pull-left">
                          <p class="bold font-montserrat text-uppercase">TOTAL  
                              <span class="text-success">**Auto plus <strong>&#8358 100</strong></span></p>
                      </div>
                      <div class="pull-right">
                          <p class="bold font-montserrat text-uppercase">&#8358 
                              <span id="total_text"><?= $form_data['total'] ?></span></p>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4 m-t-10 sm-m-t-10">
                      <input type="hidden" name="total" id="total" value="<?= $form_data['total'] ?>" />
                      <input type="hidden" name="submit_bill" value="yes"/>
                      <?php if($response_editable){ ?>
                      <button type="submit" id="submit_btn" class="btn btn-primary btn-block m-t-5" 
                              <?php if($transaction->errand->errand_status != "PENDING"){echo 'disabled';}else if($form_data['total'] == 0){echo 'disabled';} ?>>Submit</button>
                      <?php }else{
                      ?>
                      <strong class="m-t-5 text-danger">Customer already got this bill.</strong>
                      <?php
                      } ?>
                  </div>
                </div>
              </form>
                <br />
              <p class="p-b-10"><strong>NOTE</strong>: BeeQui takes a commission of <strong>10%</strong> on every transaction total amount. Additional
payment gateway fee of <strong>&#8358 100</strong> is paid by customer</p>
            </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade in <?= $tabs['actions'] ?>" id="actions">
              <div class="row">
                <div class="col-md-12">
<!--                  <h3>Follow us &amp; get updated!</h3>
                  <p>Instantly connect to what's most important to you. Follow your friends, experts, favorite celebrities, and breaking news.</p>-->
                  <br>
                </div>
              </div>
            </div>
              
            <div class="tab-pane fade in <?= $tabs['payment'] ?>" id="payment">
              <div class="row">
                <div class="col-md-12">
<!--                  <h3>Follow us &amp; get updated!</h3>
                  <p>Instantly connect to what's most important to you. Follow your friends, experts, favorite celebrities, and breaking news.</p>-->
                  <br>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
}
else{
?>
<div class="alert alert-danger">For one reason or another, your request to view this transaction was denied.</div>
<?php
}
?>

<script>
    var constantCharge = 100;
    $(document).ready(function(){
        $('.key_press').on('keydown', processTotal);
        $('.key_press').on('keyup', processTotal);
    });
    function processTotal(){
        var errand_cost = $('#errand_cost').val();
        var cash_handling_cost = $('#cash_handling_cost').val();
        var what_we_pay_you = $('#what_we_pay_you');
        var our_commission = $('#our_commission');
        var tax = $('#tax').val();
        var submit_btn = $('#submit_btn');
        var total_text = $('#total_text');
        var total_input = $('#total');
        if(isNaN(errand_cost) || isNaN(cash_handling_cost) || isNaN(tax)
                || errand_cost === "" || cash_handling_cost === "" || tax === ""){
            submit_btn.prop('disabled', true);
            what_we_pay_you.val('0');
            our_commission.val('0');
            total_text.html("<span class='text-danger'>Invalid Arithmetic</span>");
            total_input.val('0');
        }
        else{
            //then do the math here
            var totalVal = parseInt(errand_cost) + parseInt(cash_handling_cost);
            if(tax > 0){
                var taxVal = parseInt(tax)/100 * totalVal;
                totalVal += taxVal;
            }
            ourCommisionVal = 10/100 * totalVal;
            whatWePayYouVal = totalVal - ourCommisionVal;
            if(totalVal > 100){
                what_we_pay_you.val(whatWePayYouVal);
                our_commission.val(ourCommisionVal);
                total_text.html(totalVal + constantCharge);
                total_input.val(totalVal + constantCharge);
                submit_btn.prop('disabled', false);
                submit_btn.prop('disabled', false);
            }
        }
        
    }
</script>