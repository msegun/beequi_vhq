        <!-- START PAGE CONTENT -->
        <div class="content sm-gutter">
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid padding-25 sm-padding-10">

<!-- START PANEL -->
<div class="panel panel-transparent">
   <div class="panel-body">

      <h3>All States:</h3>

      <?php
      if($notification['status'] == 0){
         echo'<div class="alert alert-danger" role="alert">
                      <button class="close" data-dismiss="alert"></button>
               '.$notification['message'].'
             </div>
              ';

      }elseif($notification['status'] == 1){
         echo'<div class="alert alert-success" role="alert">
                      <button class="close" data-dismiss="alert"></button>
               '.$notification['message'].'
             </div>
              ';
      }else{

      }
      ?>


      <div class="panel-body">
         <table class="table table-hover demo-table-dynamic" id="tableWithDynamicRows">
            <thead>
            <tr>

               <th>State Name</th>
               <th>Total Cities</th>
               <th>List of Cities<br /><small>Click to delete</small></th>
               <th>Add a City</th>
               <th>Delete</th>
            </tr>
            </thead>
            <tbody>

            <?php
            foreach($states_list as $states){
                $list_of_cities = "";
                foreach($states['cities'] as $city){
                    $list_of_cities.="<a onclick='return confirm(\"Proceed with delete ?\")' class='text-danger' title='click to delete' href='".
                            SITE_PATH."states/region_remove/".$states['_id']."/".$city."'>".$city."</a>, ";
                }
               echo '
<tr>

               <td class="v-align-middle">
                  <p>' . $states['name'] . '</p>
               </td>
               
               <td class="v-align-middle">
                  <p>' . count($states['cities']) . '</p>
               </td>
               
               <td class="v-align-middle">
                  <p>' . $list_of_cities . '</p>
               </td>
               
               <td class="v-align-left">
                  <form method="post" action="" class="form-inline">
                  <div class="form-group col-xs-8"><input style="width:99%;" type="text" name="region" class="form-control input-xs"/></div>
                  <input type="hidden" name="state_id" value="'.$states['_id'].'" />
                    <div class="form-group col-xs-2"><input type="submit" name="add_region" class="btn btn-success btn-xs" value="Add"/></div>
                  </form>
               </td>
               <td>
               <div class="col-xs-2"><a onclick="return confirm(\'Proceed with delete ?\')" href="'.SITE_PATH.'states/delete/'.$states['_id'].'" class="btn btn-danger btn-xs">
                   <i class="glyphicon glyphicon-trash"></i></a></div>
               </td>
             
               
</tr>
                  ';

            }

            ?>

            </tbody>
         </table>
      </div>

   </div>
</div>


          </div>
          <!-- END CONTAINER FLUID -->
        </div>