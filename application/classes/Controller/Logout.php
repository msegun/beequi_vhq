<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Logout extends Controller {
    
    private $session;
    
    public function __construct(\Request $request, \Response $response) {
        parent::__construct($request, $response);
        $this->session = Session::instance();
    }
    
    public function action_index()
    {
        $token = $this->session->get('admin_access_token');
        if($this->session->get("identity") == 1){
            $call_path = 'auth/logout/';
        }
        else{
            $call_path = 'vendor/auth/logout/';
        }
        Request::factory(API_CALL_URL.$call_path. $token,
            array('strict_redirect' => FALSE, 'follow' => TRUE))
            ->method(Request::DELETE)
            ->execute();
        $this->session->delete('admin_access_token');
        $this->session->delete('db_id');
        $this->session->destroy();
        HTTP::redirect(SITE_PATH);
    }        
} // End Welcome
