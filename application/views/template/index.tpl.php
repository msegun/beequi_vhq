<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <meta charset="utf-8" />
  <title><?php echo $page_title; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <link rel="apple-touch-icon" href="<?php echo SITE_PATH; ?>pages/ico/60.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo SITE_PATH; ?>pages/ico/76.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo SITE_PATH; ?>pages/ico/120.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo SITE_PATH; ?>pages/ico/152.png">
  <link rel="icon" type="image/x-icon" href="favicon.ico" />
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-touch-fullscreen" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="default">
  <meta content="" name="description" />
  <meta content="" name="author" />
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <link href="<?php echo SITE_PATH; ?>assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo SITE_PATH; ?>assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo SITE_PATH; ?>assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo SITE_PATH; ?>assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="<?php echo SITE_PATH; ?>assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="<?php echo SITE_PATH; ?>assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="<?php echo SITE_PATH; ?>pages/css/pages-icons.css" rel="stylesheet" type="text/css">
  <link class="main-stylesheet" href="<?php echo SITE_PATH; ?>pages/css/pages.css" rel="stylesheet" type="text/css" />
  <!--[if lte IE 9]>
  <link href="<?php echo SITE_PATH; ?>pages/css/ie9.css" rel="stylesheet" type="text/css" />
  <![endif]-->
  <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="<?php echo SITE_PATH; ?>pages/css/windows.chrome.fix.css" />'
    }
  </script>
</head>

<body class="fixed-header">
<!-- START PAGE-CONTAINER -->
<div class="lock-container full-height">
  <!-- START PAGE CONTENT WRAPPER -->
  <?php
    $session = Session::instance();
    if($session->get("demo")){
        echo "<div class='alert alert-warning text-center'>You are on our test environment system. You can play with the platform anyhow !!!</div>";
    }
  ?>
  <div class="container-sm-height full-height sm-p-t-50">
    <div class="row row-sm-height">
      <div class="col-sm-6 col-sm-height col-middle">
        <!-- START Lock Screen User Info -->
        <div class="inline">
          <div class="thumbnail-wrapper circular d48 m-r-10 ">
            <img width="43" height="43" data-src-retina="<?= SITE_PATH ?>assets/images/bq_icon.png" data-src="assets/images/bq_icon.png" alt="" src="assets/images/bq_icon.png">
          </div>
        </div>
        <div class="inline">
          <h5 class="logged hint-text no-margin">
            Backoffice
          </h5>
          <h2 class="name no-margin">Login</h2>
        </div>
        <!-- END Lock Screen User Info -->
      </div>
      <div class="col-sm-6 col-sm-height col-middle">
        <!-- START Lock Screen Form -->

        <?php
        if($login_response['status'] == 0){
          echo'<div class="alert alert-danger" role="alert">
                      <button class="close" data-dismiss="alert"></button>
               '.$login_response['message'].'
             </div>
              ';

        }else{

        }
        ?>

        <form id="form-lock" role="form" action="" method="post">
          <div class="row">
            <div class="col-sm-12">
              <!-- START Form Control -->

              <div class="form-group form-group-default sm-m-t-30">
                <label>User ID</label>
                <div class="controls">
                    <input type="text" name="log_user_id" placeholder="User ID to unlock" class="form-control" autocomplete="false" required>
                </div>
              </div>

              <div class="form-group form-group-default sm-m-t-30">
                <label>Password</label>
                <div class="controls">
                  <input type="password" name="log_pass" placeholder="Password to unlock" class="form-control" autocomplete="false" required>
                </div>
              </div>

              <input type="hidden" name="isLog" value="yes" />

              <div class="form-group form-group-default form-group-default-select2 required">
                <label class="">Login As</label>
                <select name="log_as" class="full-width" data-placeholder="Select Identity" data-init-plugin="select2" required>
                  <optgroup label="Select login identity">
                    <option value="2">Logistics/Errand Partner</option>
                    <option value="1">Super Administrator</option>
                  </optgroup>
                </select>
              </div>


              <div class="m-t-15">
                <button type="submit" class="btn btn-primary btn-cons btn-animated from-top fa fa-unlock-alt">
                  <span>Login</span>
                </button>
                <a href="<?= SITE_PATH ?>welcome/demo" class="btn btn-info btn-cons btn-animated from-top fa fa-hospital-o">
                  <span>Demo</span>
                </a>
              </div>
              <!-- END Form Control -->
            </div>
          </div>
          <!-- START Lock Screen Notification Icons-->
          <div class="row">

          </div>
          <!-- END Lock Screen Notification Icons-->
        </form>
        <!-- END Lock Screen Form -->
      </div>
    </div>
  </div>
  <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE-CONTAINER -->
<!-- START Lock Screen Footer Content-->

<!-- BEGIN VENDOR JS -->
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="<?php echo SITE_PATH; ?>assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="<?php echo SITE_PATH; ?>assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="<?php echo SITE_PATH; ?>assets/plugins/modernizr.custom.js" type="text/javascript"></script>
<script src="<?php echo SITE_PATH; ?>assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo SITE_PATH; ?>assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo SITE_PATH; ?>assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
<script src="<?php echo SITE_PATH; ?>assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="<?php echo SITE_PATH; ?>assets/plugins/jquery-bez/jquery.bez.min.js"></script>
<script src="<?php echo SITE_PATH; ?>assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
<script src="<?php echo SITE_PATH; ?>assets/plugins/jquery-actual/jquery.actual.min.js"></script>
<script src="<?php echo SITE_PATH; ?>assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script src="<?php echo SITE_PATH; ?>assets/plugins/bootstrap-select2/select2.min.js"></script>
<script src="<?php echo SITE_PATH; ?>assets/plugins/classie/classie.js"></script>
<script src="<?php echo SITE_PATH; ?>assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
<script src="<?php echo SITE_PATH; ?>assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="<?php echo SITE_PATH; ?>pages/js/pages.min.js"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="<?php echo SITE_PATH; ?>assets/js/scripts.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->
<script>
  $(function()
  {
    $('#form-lock').validate()
  })
</script>
</body>
</html>