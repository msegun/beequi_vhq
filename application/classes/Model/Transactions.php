<?php

class Model_Transactions extends Model{
    
    private $identity;
    private $access_token;
    
    public function __construct($db_id, $token) {
        $this->identity = $db_id;
        $this->access_token = $token;
    }
    
    public function getTransactionSummary(){
        $arr = array();
        $curl_response = Model::factory('Curl')->get(API_CALL_URL.
                'transaction/summary/'.$this->identity.'/'.$this->access_token);
        $curl_response_summary = json_decode($curl_response);
        $data_array = $curl_response_summary->data;
        foreach ($data_array as $one){
            $id = $one->_id;
            $total = $one->how_many;
            $arr[$id] = $total;
        }
        return $arr;
    }
    
    public function viewTransaction($id){
        $curl_response = Model::factory('Curl')->get(API_CALL_URL.
                'transaction/view/'.$id.'/'.$this->identity.'/'.$this->access_token);
        $curl_response_summary = json_decode($curl_response);
        if(!$curl_response_summary->status){
            return false;
        }
        return $curl_response_summary->data;
    }
    
    public function billTransaction($id, $data){
        return Request::factory(API_CALL_URL.'transaction/bill/'.$id.'/'.
                $this->identity.'/'.$this->access_token,
            array('strict_redirect' => FALSE, 'follow' => TRUE))
            ->method(Request::POST)
            ->post(array('data' => $data))
            ->execute();
    }
    
    public function fetchTransaction($type, $current=1, $rowCount=10, $searchPhase=""){
        $curl_response = Model::factory('Curl')->get(API_CALL_URL.
                'transaction/fetch/'.$type.'/'.$this->identity.'/'.$this->access_token.'/'.
                $current.'/'.$rowCount.'/'.$searchPhase);
        $curl_response_summary = json_decode($curl_response, FALSE);
        //if(!$curl_response_summary->status){return false;}
        $data = $curl_response_summary->data;
        return $data;
    }
}

