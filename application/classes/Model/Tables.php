<?php defined('SYSPATH') or die('No direct script access.');

class Model_Tables extends Model{
    
    private $transaction;
    private $db_id;
    private $access_token;
    
    public function __construct($did, $access_t) {
        //parent::__construct();
        $this->db_id = $did;
        $this->access_token = $access_t;
        $this->transaction = new Model_Transactions($this->db_id, $this->access_token);
    }
    
    public function fetchTransaction($type, $current=1, $rowCount=10, $searchPhase=""){
        $response = array();
        $rowsArray = array();
        $fetched = $this->transaction->fetchTransaction($type, $current, $rowCount, $searchPhase);
        if($fetched){
            $response['current'] = $fetched->page;
            $response['rowCount'] = $fetched->limit;
            $response['total'] = $fetched->total;
            $loop = 0;
            foreach($fetched->docs as $row){
                $d = array();
                $d['id'] = $row->_id;
                $d['date'] = @$row->errand->errand_created;
                $d['batch'] = $row->transaction_batch_id;
                $d['ref'] = $row->transaction_ref;
                $d['source'] = $row->source;
                $d['from'] = $row->query->state_origin;
                $d['to'] = $row->query->state_destination;
                $d['weight'] = $row->errand->item_weight;
                $rowsArray[$loop] = $d;
                $loop++;
            }
            $response['rows'] = $rowsArray;
        }
        return $response;
    }
    
    public function fetchManageStates($current=1, $rowCount=10, $searchPhase=""){
        $response = array();
        $response['current'] = intval($current);
        $response['rowCount'] = intval($rowCount);
        //first get the total rows available
        $query = DB::select()->from("states")
                ->where("st_name", "LIKE", "%".$searchPhase."%");
        $response['total'] = intval(count($query->as_object()->execute($this->database)));
        $rowsArray = array();

        $query_main = DB::query(Database::SELECT, "SELECT states.*, regg.total_regions FROM".
                " states LEFT JOIN (SELECT re_state_id, COUNT(re_id) AS total_regions FROM ".
                "regions GROUP BY re_state_id) AS regg ON states.st_id=regg.re_state_id ".
                "WHERE st_name LIKE '%".$searchPhase."%' LIMIT ".(($current-1) * $rowCount).
                ", ".$rowCount);
        $result = $query_main->as_object()->execute($this->database);
        $loop = 0;
        foreach($result as $row){
            $d = array();
            $d['id'] = $row->st_id;
            $d['state'] = $row->st_name;
            if($row->total_regions == null){
                $d['regions'] = 0;
            }
            else{
                $d['regions'] = intval($row->total_regions);
            }
            $d['status'] = ($row->st_flag)
                    ?"<span class='text-success'>Active</span>":
                    "<span class='text-danger'>Dis-abled</span>";

            $rowsArray[$loop] = $d;
            $loop++;
        }
        $response['rows'] = $rowsArray;
        return $response;
    }
    
}