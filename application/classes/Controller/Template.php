<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Template extends Controller {

    public function before() {
        parent::before();
        if(Kohana_Request::user_agent("mobile")){
            HTTP::redirect(SITE_PATH."welcome/mobile");
        }
    }
    
    public function __construct(\Request $request, \Response $response) {
        parent::__construct($request, $response);
    }
        
} // End Template