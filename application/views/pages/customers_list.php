        <!-- START PAGE CONTENT -->
        <div class="content sm-gutter">
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid padding-25 sm-padding-10">

<!-- START PANEL -->
<div class="panel panel-transparent">
   <div class="panel-body">

      <h3>All Customers:</h3>
        <div class="panel-body">

<table id="data-table-customer" class="table table-striped table-vmiddle">
    <thead>
        <tr>
            <th data-column-id="id" data-type="numeric">ID</th>
            <th data-column-id="sender">Sender</th>
            <th data-column-id="received" data-order="desc">Received</th>
            <th data-column-id="commands" data-formatter="commands" data-sortable="false">Commands</th>
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>
        </div>

   </div>
</div>


          </div>
          <!-- END CONTAINER FLUID -->
        </div>

<script type="text/javascript">
    $(document).ready(function(){
        
        var grid_services = $("#data-table-customer").bootgrid({
            ajax: true,
            ajaxSettings: {
                method: "POST",
                cache: false
            },
            url: "<?= SITE_PATH ?>tables/customers",
            formatters: {
                "commands": function(column, row)
                {
                    return "<button type=\"button\" class=\"btn btn-xs btn-default command-disable\" data-row-id=\"" + 
                            row.id + "\" title=\"Enable/Disable\"><span class=\"zmdi zmdi-flare\"></span></button> " + 
                "<button type=\"button\" class=\"btn btn-xs btn-default command-view\" data-row-id=\"" + 
                row.id + "\" title=\"See More Info\"><span class=\"zmdi zmdi-eye\"></span></button>"+
                "<button type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\"" + 
                row.id + "\" title=\"Edit State Name\"><span class=\"zmdi zmdi-edit\"></span></button>";
                }
            }
        }).on("loaded.rs.jquery.bootgrid", function()
        {
            /* Executes after data is loaded and rendered */
            grid_services.find(".command-disable").on("click", function(e)
            {
                location.href=PARENT+"/configuration/manage_services/"+$(this).data("row-id")+"/disable-enable"
                //alert("You pressed disable on row: " + $(this).data("row-id"));
            }).end().find(".command-view").on("click", function(e)
            {
                location.href=PARENT+"/configuration/manage_services/"+$(this).data("row-id")+"/view"
                //alert("You pressed view on row: " + $(this).data("row-id"));
            }).end().find(".command-edit").on("click", function(e)
            {

                //location.href=PARENT+"/configuration/manage_states/"+$(this).data("row-id")+"/view"
                //alert("You pressed view on row: " + $(this).data("row-id"));
            });
        });
    });
</script>

