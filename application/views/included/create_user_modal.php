<!-- Modal -->
<div class="modal fade slide-up disable-scroll" id="modalSlideUp" tabindex="-1" role="dialog" aria-hidden="false">
   <div class="modal-dialog ">
      <div class="modal-content-wrapper">
         <div class="modal-content">
            <div class="modal-header clearfix text-left">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
               </button>
               <h5>Create a <span class="semi-bold">New Vendor</span></h5>
            </div>
            <div class="modal-body">

               <form id="form-register" class="p-t-15" role="form" action="<?php echo SITE_PATH; ?>vendors" method="post">
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group form-group-default">
                           <label>Vendor Name</label>
                           <input type="text" name="vendor_name" placeholder="enter vendor name" class="form-control" required>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group form-group-default">
                           <label>Vendor Email</label>
                           <input type="email" name="vendor_email" placeholder="enter vendor email" class="form-control" required>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group form-group-default">
                           <label>Vendor Password</label>
                           <input type="text" name="vendor_pass" placeholder="Minimum of 4 Charactors" class="form-control" required>
                        </div>
                     </div>
                  </div>

                  <input type="hidden" name="isAddVendor" value="yes" />
                  <button class="btn btn-primary btn-cons m-t-10" type="submit">Create Vendor</button>
               </form>

            </div>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
</div>