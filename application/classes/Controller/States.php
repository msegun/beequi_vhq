<?php defined('SYSPATH') or die('No direct script access.');

class Controller_States extends Controller_Template {

    private $session;
    private $access_token;
    private $logged_user;
    
    public function __construct(\Request $request, \Response $response) {
        parent::__construct($request, $response);
        $this->session = Session::instance();
        $this->access_token = $this->session->get('admin_access_token');
        $this->logged_user = $this->session->get('user_id');
        if($this->session->get('identity') == 1){
            $this->header_view = 'included/home_header';
        }
        else{
            $this->header_view = 'included/vendor_header';
        }
    }
    
    public function action_index(){
        if(!$this->access_token){
            HTTP::redirect(SITE_PATH . "logout");
        }
        if ($this->request->post('isAddState')) {
            $state_name = $this->request->post('state_name');

            if (empty($state_name)) {
                $notification['status'] = 0;
                $notification['message'] = "Empty field(s)";
            } else { // send the details to the API for processing
                // This uses POST
                $request = Request::factory(API_CALL_URL.'states/add/' . $this->access_token,
                        array('strict_redirect' => FALSE, 'follow' => TRUE))
                        ->method(Request::POST)
                        ->post(array('state' => $state_name))
                        ->execute();
                $response = json_decode($request->body(), TRUE);

                $add_status = ($response['status']);

                if(!$add_status){
                        $add_message = ($response['data']['errmsg']);
                }else{
                        $add_message = $response['data']['name'].' state added successfully';
                }

                $notification['status'] = $add_status;
                $notification['message'] = $add_message;
            }

        }else{
            $notification['status'] = 5;
            $notification['message'] = "";
        }
        
        if($this->request->post('add_region')){
            $state_id = $this->request->post('state_id');
            $region = ucfirst($this->request->post('region'));
            $request = Request::factory(API_CALL_URL.'states/region/add/' . $this->access_token,
                    array('strict_redirect' => FALSE, 'follow' => TRUE))
                    ->method(Request::POST)
                    ->post(array('state_id' => $state_id, 'region' => $region))
                    ->execute();
            $response = json_decode($request->body(), TRUE);
            if($response['status']){
                $notification['status'] = 1;
                $notification['message'] = "Operation Success";
            }
            else{
                $notification['status'] = 0;
                $notification['message'] = "Operation Failed";     
            }
        }

        //display the states lists

        $curl_response = Model::factory('Curl')->get(API_CALL_URL.'states/list/#');

        $curl_response = json_decode($curl_response, TRUE);

        $states_response['status'] = $curl_response['status'];
        $states_data = $curl_response['data'];

        $title = "States";

        $this->view = View::factory('template/home.tpl')
                ->bind('title', $title);

        $this->view->set("dynamic_view", View::factory('pages/states_list')
                ->bind('notification', $notification)
                ->bind("states_list", $states_data));

        $this->view->set("home_header_content", View::factory($this->header_view)
                ->bind('user_type', $this->logged_user)->set("manage_states_active", "active"));


        //include the footer
        $this->view->set("footer", View::factory('included/footer'));


        //set all the modal dat will be used by this action here
        $this->view->set("create_user_modal_content", View::factory('included/create_user_modal'));
        $this->view->set("create_state_modal_content", View::factory('included/create_state_modal'));

        $this->response->body($this->view); //will comment this later
    }
    
    public function action_delete(){
        $state_id = $this->request->param("id");
        $request = Request::factory(API_CALL_URL.'states/remove/' . $this->access_token,
                array('strict_redirect' => FALSE, 'follow' => TRUE))
                ->method(Request::DELETE)
                ->post(array('state_id' => $state_id))
                ->execute();
        HTTP::redirect(SITE_PATH.'states');
    }
    
    public function action_region_remove(){
        $state_id = $this->request->param("id");
        $region = $this->request->param("id2");
        $request = Request::factory(API_CALL_URL.'states/region/remove/' . $this->access_token,
                array('strict_redirect' => FALSE, 'follow' => TRUE))
                ->method(Request::DELETE)
                ->post(array('state_id' => $state_id, 'region' => $region))
                ->execute();
        HTTP::redirect(SITE_PATH.'states');
    }
}