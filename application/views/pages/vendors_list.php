        <!-- START PAGE CONTENT -->
        <div class="content sm-gutter">
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid padding-25 sm-padding-10">

<!-- START PANEL -->
<div class="panel panel-transparent">
   <div class="panel-body">

      <h3>All Vendors:</h3>

      <?php
      if($notification['status'] == 0){
         echo'<div class="alert alert-danger" role="alert">
                      <button class="close" data-dismiss="alert"></button>
               '.$notification['message'].'
             </div>
              ';

      }elseif($notification['status'] == 1){
         echo'<div class="alert alert-success" role="alert">
                      <button class="close" data-dismiss="alert"></button>
               '.$notification['message'].'
             </div>
              ';
      }else{

      }
      ?>


      <div class="panel-body">
         <table class="table table-hover demo-table-dynamic" id="tableWithDynamicRows">
            <thead>
            <tr>

               <th>Vendor Name</th>
               <th>Created At</th>
               <th>Login Email</th>
               <th>Status</th>
               <th>Rating</th>
               <th>Manage</th>
            </tr>
            </thead>
            <tbody>

            <?php
            foreach($vendors_list as $vendors){
                try{
                $rating = (intval(@$vendors['rating']['total_points'])/intval(@$vendors['rating']['raters_count']))*5;
                }
                catch(ErrorException $e){ $rating = "<span class='text-warning'>Unrated</span>"; }
                $status = "<span class='text-success'>Active</span>";
                if(!$vendors['active']){
                    $status = "<span class='text-danger'>Suspended</span>";
                }
                
               echo '
<tr>

               <td class="v-align-middle">
                  <p>' . $vendors['vendor_name'] . '</p>
               </td>
               <td class="v-align-middle">
                  <p>' . $vendors['created']. '</p>
               </td>
               <td class="v-align-middle">
                  <p>' . $vendors['login_credentials']['email'] . '</p>
               </td>
               <td class="v-align-middle">
                  <p>' .$status. '</p>
               </td>
               <td class="v-align-middle">
                  <p>' .$rating. '</p>
               </td>
               <td class="v-align-middle">
                  <p><a href="'.SITE_PATH.'vendors/profile/'.$vendors['_id'].'">View</a></p>
               </td>
</tr>
                  ';

            }

            ?>

            </tbody>
         </table>
      </div>

   </div>
</div>


          </div>
          <!-- END CONTAINER FLUID -->
        </div>