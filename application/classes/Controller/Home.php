<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Home extends Controller_Template {

    private $session;
    private $db_id;
    private $business_name;
    
    private $pending_count = 0;
    private $processing_count = 0;
    private $completed_count = 0;
    
    private $transactions;


    public function __construct(\Request $request, \Response $response) {
        parent::__construct($request, $response);
        $this->session = Session::instance();
        $this->access_token = $this->session->get('admin_access_token');
        $this->logged_user = $this->session->get('user_id');
        $this->db_id = $this->session->get('db_id');
        if($this->session->get('identity') == 1){
            $this->header_view = 'included/home_header';
            $this->business_name = 'Super Administrator';
        }
        else{
            $curl_response = Model::factory('Curl')->get(API_CALL_URL.'vendor/self/view/'.$this->access_token.'/'.$this->logged_user);
            $curl_response_basic = json_decode($curl_response);
            $this->business_name = $curl_response_basic->data->vendor_name;
            $this->header_view = 'included/vendor_header';
        }
        $this->transactions = new Model_Transactions($this->db_id, $this->access_token);
    }
    
	public function action_index()
	{
            if(!$this->session->get('admin_access_token')){
                HTTP::redirect(SITE_PATH.'logout');
            }
            
            $arr_tranx = $this->transactions->getTransactionSummary();
            $this->pending_count = array_key_exists("PENDING", $arr_tranx)?$arr_tranx['PENDING']:0;
            $this->processing_count = array_key_exists("PROCESSING", $arr_tranx)?$arr_tranx['PROCESSING']:0;
            $this->completed_count = array_key_exists("COMPLETED", $arr_tranx)?$arr_tranx['COMPLETED']:0;
            
            $title = "Welcome ".$this->logged_user;

            $this->view = View::factory('template/home.tpl')
                    ->bind('title', $title);
            
            $this->view->set("dynamic_view", View::factory('pages/home')
                    ->set("business_name", $this->business_name)
                    ->set("pending_jobs", $this->pending_count)
                    ->set("processing_jobs", $this->processing_count)
                    ->set("completed_jobs", $this->completed_count));

            $this->view->set("home_header_content", View::factory($this->header_view)
                    ->bind('user_type', $this->logged_user));

            //include the footer
            $this->view->set("footer", View::factory('included/footer'));


            //set all the modal dat will be used by this action here
            $this->view->set("create_user_modal_content", View::factory('included/create_user_modal'));
            $this->view->set("create_state_modal_content", View::factory('included/create_state_modal'));

            $this->response->body($this->view); //will comment this later


	}
        
        
} // End Welcome
